"""
Create CustomList – the linked list of values of random type, which size changes dynamically and has an ability to index
elements.

The task requires implementation of the following functionality:
• Create the empty user list and the one based on enumeration of values separated by commas. The elements are stored
in form of unidirectional linked list. Nodes of this list must be implemented in class Item.
    Method name: __init__(self, *data) -> None;
• Add and remove elements.
    Method names: append(self, value) -> None - to add to the end,
                add_start(self, value) -> None - to add to the start,
                remove(self, value) -> None - to remove the first occurrence of given value;
• Operations with elements by index. Negative indexing is not necessary.
    Method names: __getitem__(self, index) -> Any,
                __setitem__(self, index, data) -> None,
                __delitem__(self, index) -> None;
• Receive index of predetermined value.
    Method name: find(self, value) -> Any;
• Clear the list.
    Method name: clear(self) -> None;
• Receive lists length.
    Method name: __len__(self) -> int;
• Make CustomList iterable to use in for-loops;
    Method name: __iter__(self);
• Raise exceptions when:
    find() or remove() don't find specific value
    index out of bound at methods __getitem__, __setitem__, __delitem__.


Notes:
    The class CustomList must not inherit from anything (except from the object, of course).
    Function names should be as described above. Additional functionality has no naming requirements.
    Indexation starts with 0.
    List length changes while adding and removing elements.
    Inside the list the elements are connected as in a linked list, starting with link to head.
"""


class Item:
    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self):
        return self.data

    def __iter__(self):
        n = self
        while n is not None:
            yield n
            n = n.next

    def __len__(self) -> int:
        n = self
        count = 0
        while n is not None:
            count += 1
            n = n.next
        return count

class CustomList:
    def __init__(self, *data):
        self.ourlist = None
        if data:
            for el in data:
                self.append(el)

    def append(self, value) -> None:
        new_item = Item(value)
        if self.ourlist is None:
            self.ourlist = new_item
            return
        n = self.ourlist
        while n.next is not None:
            n = n.next
        n.next = new_item

    def add_start(self, value) -> None:
        new_item = Item(value)
        new_item.next = self.ourlist
        self.ourlist = new_item

    def remove(self, value) -> None:
        n = self.ourlist
        self.ourlist = None
        flag = 1
        while n is not None:
            if n.data == value:
                flag = 0
            else:
                self.append(n.data)
            n = n.next
        if flag:
            raise ValueError

    def __getitem__(self, index):
        n = self.ourlist
        index_ = 0
        while n is not None:
            if index_ == index: return n.data
            index_ += 1
            n = n.next
        raise IndexError

    def __setitem__(self, index, data) -> None:
        n = self.ourlist
        if len(self.ourlist) - 1 < index or index < 0: raise IndexError
        self.ourlist = None
        index_ = 0
        while n is not None:
            if index_ == index:
                self.append(data)
            else:
                self.append(n.data)
            index_ += 1
            n = n.next

    def __delitem__(self, index) -> None:
        n = self.ourlist
        if len(self.ourlist) - 1 < index or index < 0: raise IndexError
        self.ourlist = None
        index_ = 0
        while n is not None:
            if index_ != index:
                self.append(n.data)
            index_ += 1
            n = n.next

    def find(self, value):
        n = self.ourlist
        index = 0
        while n is not None:
            if n.data == value: return index
            index += 1
            n = n.next
        raise ValueError

    def clear(self) -> None:
        self.ourlist = None

    def __len__(self) -> int:
        n = self.ourlist
        count = 0
        while n is not None:
            count += 1
            n = n.next
        return count

    def __iter__(self):
        n = self.ourlist
        while n is not None:
            yield n
            n = n.next

    def __repr__(self):
        s = ''
        for el in self.ourlist:
            if not s:
                s = str(el.data)
                continue
            s = s + ', ' + str(el.data)
        return s

if __name__ == '__main__':
    a = CustomList(1, 2)
    a.append('a')
    a.append('b')
    a.append('c')
    print(a.find('a'))
    a.remove('c')
    print(a)
    del a[0]
    print(a)
    print(a[0])
    a[0] = 'd'
    print(a)
    for el in a:
        print(el)
    a.clear()
    print(len(a))
