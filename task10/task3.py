"""
File `data/students.csv` stores information about students in CSV format.
This file contains the student’s names, age and average mark.

1. Implement a function get_top_performers which receives file path and
returns names of top performer students.
Example:
def get_top_performers(file_path, number_of_top_students=5):
    pass

print(get_top_performers("students.csv"))

Result:
['Teresa Jones', 'Richard Snider', 'Jessica Dubose', 'Heather Garcia',
'Joseph Head']

2. Implement a function write_students_age_desc which receives the file path
with students info and writes CSV student information to the new file in
descending order of age.
Example:
def write_students_age_desc(file_path, output_file):
    pass

Content of the resulting file:
student name,age,average mark
Verdell Crawford,30,8.86
Brenda Silva,30,7.53
...
Lindsey Cummings,18,6.88
Raymond Soileau,18,7.27
"""

import csv

def get_top_performers(file_path, number_of_top_students=5):
    with open(file_path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        lstudents = list(csv_reader)

    lstudents = lstudents[1:]
    lstudents.sort(key=lambda el: float(el[2]), reverse=True)
    return [lstudents[i][0] for i in range(number_of_top_students)]

def write_students_age_desc(file_path, output_file):
    with open(file_path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        lstudents = list(csv_reader)

    with open(output_file, 'w') as csv_file:
        csv_file.write(','.join(lstudents[0])+'\n')
        lstudents = lstudents[1:]
        lstudents.sort(key=lambda el: float(el[1]), reverse=True)
        for el in lstudents:
            csv_file.write(','.join(el) + '\n')

if __name__ == '__main__':
    print(get_top_performers('1.txt'))
    print(write_students_age_desc('1.txt', '2.txt'))
