"""Implement a function `most_common_words(file_path: str, top_words: int) -> list`
which returns most common words in the file.
<file_path> - system path to the text file
<top_words> - number of most common words to be returned

Example:

print(most_common_words(file_path, 3))
>> ['donec', 'etiam', 'aliquam']
> NOTE: Remember about dots, commas, capital letters etc.
"""

import re
from collections import Counter

def most_common_words(file_path, top_words):
    with open(file_path) as f:
        text = f.read()
    lwords = re.split(r'[!?,.() \n ]', text)
    lwords = [word.lower() for word in lwords if word]
    return [el[0] for el in Counter(lwords).most_common(top_words)]


if __name__ == '__main__':
    print(most_common_words('1.txt', 3))
