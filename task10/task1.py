"""
Implement a function `sort_names(input_file_path: str, output_file_path: str) -> None`, which sorts names from
`file_path` and write them to a new file `output_file_path`. Each name should start with a new line as in the
following example:
Example:

Adele
Adrienne
...
Willodean
Xavier
"""


def sort_names(input_file_path: str, output_file_path: str) -> None:
    with open(input_file_path) as f:
        lname = f.readlines()
        lname.sort()

    with open(output_file_path, 'w') as f:
        f.writelines(lname)

if __name__ == '__main__':
    sort_names('1.txt', '2.txt')