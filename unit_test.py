from task3 import task2
import argparse
import unittest

class TestMethods(unittest.TestCase):
    def setUp(self):
        self.from_roman_numerals = task2.from_roman_numerals
        parser = argparse.ArgumentParser()
        self.args = parser.parse_args()

    def test_from_roman_numerals_Equal(self):
        self.args.N = 'IX'
        self.assertEqual(self.from_roman_numerals(self.args), 9)

    def test_from_roman_numerals_ValueError(self):
        self.args.N = 'IXaa'
        with self.assertRaises(TypeError):
            self.from_roman_numerals(self.args)

if __name__ == '__main__':
    unittest.main()