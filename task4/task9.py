"""
For a positive integer n calculate the result value, which is equal to the sum
of the odd numbers of n.

Example,
n = 1234 result = 4
n = 246 result = 0

Write it as function.

Note:
Raise TypeError in case of wrong data type or negative integer;
Use of 'functools' module is prohibited, you just need simple for loop.
"""


def sum_odd_numbers(n: int) -> int:
    if not isinstance(n, int): raise TypeError
    if n < 1: raise TypeError
    n = str(n)
    if not n.isdigit(): raise TypeError
    n = map(int, tuple(n))
    return sum(el for el in n if el % 2)

if __name__ == '__main__':
    print(sum_odd_numbers(0))
