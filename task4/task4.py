"""
Implement a function `split_by_index(string: str, indexes: List[int]) -> List[str]`
which splits the `string` by indexes specified in `indexes`. 
Only positive index, larger than previous in list is considered valid.
Invalid indexes must be ignored.

Examples:
```python
>> split_by_index("pythoniscool,isn'tit?", [6, 8, 12, 13, 18])
["python", "is", "cool", ",", "isn't", "it?"]

>> split_by_index("pythoniscool,isn'tit?", [6, 8, 8, -4, 0, "u", 12, 13, 18])
["python", "is", "cool", ",", "isn't", "it?"]

>> split_by_index("no luck", [42])
["no luck"]
```
"""


def split_by_index(string, indexes):
    if not string: return []
    indexes = [el for el in indexes if isinstance(el, int) and el > 0]
    indexes.insert(0, 0)
    indexes.append(len(string))
    indexes = [indexes[i] for i in range(len(indexes)) if indexes[i] > indexes[i-1]]
    indexes = list(set(indexes))
    indexes.sort()
    list_input_str = []
    beg = 0
    for index in indexes:
        list_input_str.append(string[beg:index])
        beg = index
    return list_input_str


if __name__ == '__main__':
    print(split_by_index("pythoniscool,isn'tit?  ", [6, 8, 12, 13, 18]))
    print(split_by_index("pythoniscool,isn'tit?", [6, 8, 8, -4, 0, "u", 12, 13, 18]))
    print(split_by_index("no luck", []))
    print(split_by_index("wewillrockyou", [2, 5, 8]))