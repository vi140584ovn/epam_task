"""04 Task 1.1
Implement a function which receives a string and replaces all " symbols with ' and vise versa. The
function should return modified string.
Usage of any replacing string functions is prohibited.
"""


def swap_quotes(string: str) -> str:
    return '"'.join(["'".join(el.split('"')) for el in string.split("'")])

if __name__ == "__main__":
    print(swap_quotes("""aaadf''asdf""asdf"""))
