"""
Task04_3

Implement a function which works the same as str.split

Note:
Usage of str.split method is prohibited
Raise ValueError in case of wrong data type
"""


def split_alternative(str_to_split: str, delimiter=' ') -> list:
    if not isinstance(str_to_split, str):
        raise ValueError
    list_str = []
    while True:
        spl_beg = str_to_split.find(delimiter)
        if spl_beg < 0:
            list_str.append(str_to_split)
            break
        list_str.append(str_to_split[0:spl_beg])
        str_to_split = str_to_split[spl_beg + len(delimiter):len(str_to_split)]
    return list_str


if __name__ == '__main__':
    print(split_alternative('asdf asdf', 'a'))

