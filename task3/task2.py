"""
Write a function converting a Roman numeral from a given string N into an Arabic numeral.
Values may range from 1 to 100 and may contain invalid symbols.
Invalid symbols and numerals out of range should raise ValueError.

Numeral / Value:
I: 1
V: 5
X: 10
L: 50
C: 100

Example:
N = 'I'; result = 1
N = 'XIV'; result = 14
N = 'LXIV'; result = 64

Example of how the task should be called:
python3 task_3_ex_2.py LXIV

Note: use `argparse` module to parse passed CLI arguments
"""

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("N", type=str, help="")


def from_roman_numerals(args):
    d_rome = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100}
    l_rome = ['I', 'V', 'X', 'L', 'C']
    d_rome_min = {'IV': 4, 'IX': 9, 'XL': 40, 'XC': 90}
    N = args.N
    if not N or set(list(N)) - set(l_rome): raise ValueError
    arabic = []
    for el in d_rome_min:
        if el in N:
            arabic.append(d_rome_min[el])
            N = N.replace(el, '')
    for el in N:
        arabic.append(d_rome[el])
    if sum(arabic) > 100: raise ValueError
    return sum(arabic)


def main():
    args = parser.parse_args()
    print(from_roman_numerals(args))


if __name__ == "__main__":
    main()
