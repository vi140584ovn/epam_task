"""
Task 2_3

You are given n bars of gold with weights: w1, w2, ..., wn and bag with capacity W.
There is only one instance of each bar and for each bar you can either take it or not
(hence you cannot take a fraction of a bar). Write a function that returns the maximum weight of gold that fits
into a knapsack's capacity.

The first parameter contains 'capacity' - integer describing the capacity of a knapsack
The next parameter contains 'weights' - list of weights of each gold bar
The last parameter contains 'bars_number' - integer describing the number of gold bars
Output : Maximum weight of gold that fits into a knapsack with capacity of W.

Note:
Use the argparse module to parse command line arguments. You don't need to enter names of parameters (i. e. -capacity)
Raise ValueError in case of false parameter inputs
Example of how the task should be called:
python3 task3_1.py -W 56 -w 3 4 5 6 -n 4
"""
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-W', type=int,  help='')
parser.add_argument('-w', type=int, nargs='+', help='')
parser.add_argument('-n', type=int, help='')


def bounded_knapsack(args):
    if args.W < 1 or args.n < 1 or min(args.w) < 1:
        raise ValueError

    result = []
    for i in range(len(args.w)):
        if args.w[i] > args.W:
            continue
        main_m = [args.w[i]]
        for j in range(i+1, len(args.w)):
            main_m.append(args.w[j])
            if sum(main_m) > args.W:
                main_m.pop()
        if sum(result) < sum(main_m):
            result = main_m[:]
    if sum(result) == 0:
        raise ValueError
    return sum(result)

def main():
    args = parser.parse_args()
    print(bounded_knapsack(args))


if __name__ == '__main__':
    main()
