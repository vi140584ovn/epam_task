"""01-Task1-Task2
Write a Python-script that performs the standard math functions on the data. The name of function and data are
set on the command line when the script is run.
The script should be launched like this:
$ python my_task.py add 1 2

Notes:
Function names must match the standard mathematical, logical and comparison functions from the built-in libraries.
The script must raises all happened exceptions.
For non-mathematical function need to raise NotImplementedError.
Use the argparse module to parse command line arguments. Your implementation shouldn't require entering any
parameters (like -f or --function).
"""

import argparse
import math
import operator

parser = argparse.ArgumentParser()
parser.add_argument("operator", type=str, help="")
parser.add_argument("number", type=float, nargs='+', help="")

def calculate(args):
    op_func = {}
    for el in dir(operator):
        if '__' not in el:
            op_func[el] = getattr(operator, el)
    for el in dir(math):
        if '__' not in el:
            op_func[el] = getattr(math, el)
    if op := op_func.get(args.operator):
        return op(*args.number)
    raise NotImplementedError


def main():
    args = parser.parse_args()
    print(calculate(args))

if __name__ == '__main__':
    main()