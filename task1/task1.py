"""01-Task1-Task1
Write a Python-script that performs simple arithmetic operations: '+', '-', '*', '/'. The type of operator and
data are set on the command line when the script is run.
The script should be launched like this:
$ python my_task.py 1 * 2

Notes:
For other operations need to raise NotImplementedError.
Do not dynamically execute your code (for example, using exec()).
Use the argparse module to parse command line arguments. Your implementation shouldn't require entering any
parameters (like -f or --function).
"""

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("number_1", type=float, help="first")
parser.add_argument("operator", type=str, help="*/-+")
parser.add_argument("number_2", type=float, help="second")

def calculate(args):
    dict_op = {'-': args.number_1.__sub__(args.number_2), '+': args.number_1.__add__(args.number_2),
               '/': args.number_1.__truediv__(args.number_2), '*': args.number_1.__mul__(args.number_2)}
    if args.operator in ('-', '+', '/', '*'):
        return dict_op[args.operator]
    raise NotImplementedError

def main():
    args = parser.parse_args()
    print(calculate(args))


if __name__ == '__main__':
    main()
