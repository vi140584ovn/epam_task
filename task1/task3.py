""" Write a Python-script that determines whether the input string is the correct entry for the
'formula' according EBNF syntax (without using regular expressions).
Formula = Number | (Formula Sign Formula)
Sign = '+' | '-'
Number = '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9'
Input: string
Result: (True / False, The result value / None)

Example,
user_input = '1+2+4-2+5-1' result = (True, 9)
user_input = '123' result = (True, 123)
user_input = 'hello+12' result = (False, None)
user_input = '2++12--3' result = (False, None)
user_input = '' result = (False, None)

Example how to call the script from CLI:
python task_1_ex_3.py 1+5-2

Hint: use argparse module for parsing arguments from CLI
"""
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("user_input", type=str)


def check_formula(user_input):
    list_el = ' - '.join(' + '.join(user_input.split('+')).split('-')).split()
    dict_op = {'-': lambda arg1, arg2: arg1 - arg2,
               '+': lambda arg1, arg2: arg1 + arg2}
    try:
        if not list_el or list_el[-1] in ('+', '-'):
            raise NotImplementedError
        for i in range(0, len(list_el), 2):
            if list_el[i] in ('+', '-') or not list_el[i].isdigit():
                raise NotImplementedError
        op = False
        for el in list_el:
            if op:
                suma = op(suma, int(el))
                op = False
                continue
            if op := dict_op.get(el):
                continue
            suma = int(el)
        return (True, suma)
    except NotImplementedError:
        return (False, None)


def main():
    args = parser.parse_args()
    print(check_formula(args.user_input))


if __name__ == '__main__':
    main()
